module.exports = function(config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine'],
        port: 9876,
        colors: false,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['ChromeHeadless'],
       
        singleRun: false,
        autoWatchBatchDelay: 300,
  
        reporters: ['spec'],

        files: [
          'node_modules/jquery/dist/jquery.js',
          'src/js/*',
          'tests/*'
        ],
    });
  };
  