# Library xivo-solutions-cti-lib

XiVO Solutions CTI library made for communication in the browser.

# How to use :

Simply install it via NPM and add it at the root of your application. You will be able to use Cti and xc_webrtc through your project.
More details to come...

# How to dev :

Npm commands :
* "npm run test" to start unit tests.
* "npm run build" to create index.js using webpack.