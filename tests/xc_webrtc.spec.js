describe("xc_webrtc ", function() {

    var autoAnswerHeaders = [ {s_name: 'Alert-Info', s_value: 'xivo-autoanswer'} ];

    var lineHandler = null;
    var ctiCmdHandler = null;
    var generalEventListener;
    var sessionEventListener;
    var readyCallback;
    var sessionType;
    var sessionConfig;
    var StreamUpdateHandler;
    var session = {
        register: function() {},
        unregister: function() {},
        getId: function() { return id; },
        call: function(destination) {return 0; },
        setConfiguration: function(config) { sessionConfig = config; },
        addEventListener: function(type, listener) {},
        removeEventListener: function(stype) {}
    };
    var sipCallId = "456546-df456@192.168";

    var stackConfig = {};
  
    var stack = {
        newSession: function(type, config) {
            sessionType = type;
            sessionConfig = config;
            displayName = 'username';
            id = 'outSessionId';
            if (type === 'call-audiovideo') {
                session.o_session = {get_stream_local: function() {return 0; }, media: {e_type: {s_name: xc_webrtc.mediaType.AUDIOVIDEO }}};
            }
            else {
                session.o_session = {get_stream_local: function() {return 0; }, media: {e_type: {s_name: xc_webrtc.mediaType.AUDIO }}};
            }
            return session;
        },
        start: function() {},
        stop: function() { return 0; },
        removeEventListener: function(stype) {},
    };

    function StackMock(config) {
      generalEventListener = config.events_listener.listener;
      stackConfig = config;
      return stack;
    };

    var cti = {
        setHandler: function(msgType, handler) {
            switch(msgType) {
              case Cti.MessageType.LINECONFIG: lineHandler = handler;
              case Cti.MessageType.WEBRTCCMD: ctiCmdHandler = handler;
            }
        },
        unsetHandler: function(msgType, handler) {
            switch(msgType) {
              case Cti.MessageType.LINECONFIG: lineHandler = null;
              case Cti.MessageType.WEBRTCCMD: ctiCmdHandler = null;             }
        },
        getConfig: function() {},
        attendedTransfer: jasmine.createSpy(),
        completeTransfer: jasmine.createSpy()
    };

    var sipml = {
        init: function(readyCB, failedCB) {
            readyCallback = readyCB;
        },
        isReady: function() { return false; },
        Stack: StackMock,
        setDebugLevel: function() {},
    };

    function initStack() {
        xc_webrtc.init('username', true, 8090);
    };

    function initIncomingSession(headers, media) {
        var callback = {
            newCall: jasmine.createSpy()
        };
        xc_webrtc.setHandler(xc_webrtc.MessageType.INCOMING, callback.newCall);
        return getEvent(headers, 13, media);
    };

    function initNextIncomingSession(headers, id) {
        return getEvent(headers, id);
    };

    function getEvent(headers, id, media) {
        var mediaValue = ((typeof media === 'undefined') ? 'audio' : media);
        var event = {
            type: 'i_new_call',
            o_event: {
                o_session: {o_uri_from: {s_display_name: 'username'}},
                o_message: {ao_headers: headers, o_hdr_Call_ID: {s_value: sipCallId}}
            },
            newSession: {
                accept: function(callConfig) {
                    sessionEventListener = callConfig.events_listener.listener;
                    return 0;
                },
                hold: function() { return 0; },
                resume: function() { return 0; },
                id: id,
                getId: function() { return this.id; },
                dtmf: function() {return 0; },
                addEventListener: jasmine.createSpy(),
                setConfiguration: jasmine.createSpy(),
                o_session: {media: {e_type: {s_name: mediaValue }}}
            }
        };
        spyOn(event.newSession, 'hold').and.callThrough();
        spyOn(event.newSession, 'resume').and.callThrough();
        spyOn(event.newSession, 'dtmf').and.callThrough();
        return event;
    };

    getProgressEvent = function(id, media) {
        var event = getEvent([], 'outSessionId', 'audio/video');
        event.type = 'i_ao_request';
        event.session = event.newSession;
        event.getSipResponseCode = function() { return 183; };
        event.o_event.o_session.o_uri_from = { s_diplay_name: 'test', s_user_name: 'test'};
        return event;
    };

    function getConnectedEvent(id, media) {
        return {
            type: 'connected',
            session: getEvent([], id, media).newSession,
            o_event: {
                o_session: {
                    o_uri_to: {s_user_name: 'to_name'},
                    o_uri_from: {s_user_name: 'from_name'}
                },
                o_message: {o_hdr_Call_ID: {s_value: sipCallId}}
            }
        };
    };

    function getResumedEvent(id) {
        var sid; id ? sid = id: sid = 'outSessionId';
        var event = getEvent([], sid);
        event.type = 'm_local_resume_ok';
        event.session = event.newSession;
        return event;
    };

    function getHoldedEvent(id) {
        var sid; id ? sid = id: sid = 'outSessionId';
        var event = getEvent([], sid);
        event.type = 'm_local_hold_ok';
        event.session = event.newSession;
        return event;
    };

    function getTerminatedEvent(id) {
        var terminatedEvent = getEvent([], id);
        terminatedEvent.type = 'terminated';
        terminatedEvent.session = terminatedEvent.newSession;
        return terminatedEvent;
    };

    function initFirstCall(media) {
        var firstCallEvent = initIncomingSession(autoAnswerHeaders, media);
        spyOn(firstCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(firstCallEvent);
        expect(firstCallEvent.newSession.accept).toHaveBeenCalled();
        return firstCallEvent.newSession;
    };

    function sendConnectedEvent(id, media) {
        sessionEventListener(getConnectedEvent(id, media));
    };

    beforeEach(function() {
        jasmine.clock().install();
        xc_webrtc.injectTestDependencies(cti, sipml);
        spyOn(cti, 'setHandler').and.callThrough();
        spyOn(cti, 'getConfig');
        spyOn(sipml, 'init').and.callThrough();
        spyOn(stack, 'start');
        spyOn(stack, 'stop').and.callThrough();
        spyOn(stack, 'newSession').and.callThrough();
        spyOn(stack, 'removeEventListener');
        spyOn(session, 'register');
        spyOn(session, 'unregister');
        spyOn(session, 'removeEventListener');
    });

    afterEach(function() {
        jasmine.clock().uninstall();
    });

    it('should request line config and subscribe to WebRTC CTI commands', function () {
        initStack();
        expect(cti.setHandler.calls.count()).toEqual(2);
        expect(cti.setHandler.calls.argsFor(0)[0]).toEqual(Cti.MessageType.LINECONFIG);
        expect(cti.setHandler.calls.argsFor(1)[0]).toEqual(Cti.MessageType.WEBRTCCMD);
        expect(cti.getConfig).toHaveBeenCalledWith('line');
    });

    it('should init stack when on line config', function () {
        initStack();
        var lineCfg = {name: "lineName", password: "password", xivoIp: "xivoIP"};
        lineHandler(lineCfg);
        expect(sipml.init).toHaveBeenCalled();
    });

    it('should init by line config', function() {
        var lineCfg = {name: "lineName", password: "password", xivoIp: "xivoIP"};
        xc_webrtc.initByLineConfig(lineCfg, 'name', true, 8090);
        expect(cti.setHandler.calls.count()).toEqual(1);
        expect(cti.setHandler.calls.argsFor(0)[0]).toEqual(Cti.MessageType.WEBRTCCMD);
        expect(sipml.init).toHaveBeenCalled();
    })

    it('should request register when correctly initiated', function () {
        initStack();
        var linecfg = {name: "lineName", password: "password", xivoIp: "xivoIP"};
        lineHandler(linecfg);
        expect(sipml.init).toHaveBeenCalled();
        readyCallback();
        expect(stack.start).toHaveBeenCalled();
        startedEvent = {type: 'started'};
        generalEventListener(startedEvent);
        expect(stack.newSession).toHaveBeenCalled();
        expect(sessionType).toBe('register');
        expect(session.register).toHaveBeenCalled();
    });

    it('should request retry register when unregistered immediately and wait before next try', function () {
        initStack();
        var linecfg = {name: "lineName", password: "password", xivoIp: "xivoIP"};
        lineHandler(linecfg);
        readyCallback();
        startedEvent = {type: 'started'};
        generalEventListener(startedEvent);
        expect(stack.newSession).toHaveBeenCalled();
        sessionConfig.events_listener.listener({type: 'terminated'});
        jasmine.clock().tick(1);
        expect(stack.newSession.calls.count()).toEqual(2);
        sessionConfig.events_listener.listener({type: 'terminated'});
        jasmine.clock().tick(xc_webrtc.getRegisterTimeoutStep() * 1000 + 1);
        expect(stack.newSession.calls.count()).toEqual(3);
        expect(sessionType).toBe('register');
    });

    it('initByLineConfig should init sipml', function () {
        var lineCfg = {name: "lineName", password: "password", xivoIp: "xivoIP"};
        xc_webrtc.initByLineConfig(lineCfg, 'username', true, 8090);
        expect(sipml.init).toHaveBeenCalled();
    });

  it('initByLineConfig should init sipml only once', function () {
    var lineCfg = {name: "lineName", password: "password", xivoIp: "xivoIP"};
    xc_webrtc.initByLineConfig(lineCfg, 'username', true, 8090);
    expect(sipml.init).toHaveBeenCalled();
    var backupFn = sipml.isReady;
    sipml.isReady = function() {return true;};
    sipml.init.calls.reset();
    xc_webrtc.initByLineConfig(lineCfg, 'username', true, 8090);
    expect(sipml.init).not.toHaveBeenCalled();
    sipml.isReady = backupFn;
  });

  it('initByLineConfig should start stack when ready callback is triggered', function () {
    var lineCfg = {name: "lineName", password: "password", xivoIp: "xivoIP"};
    xc_webrtc.initByLineConfig(lineCfg, 'username', true, 8090);
    expect(sipml.init).toHaveBeenCalled();
    var backupFn = sipml.isReady;
    sipml.isReady = function() {return true;};
    readyCallback();
    expect(stack.start).toHaveBeenCalled();
    sipml.isReady = backupFn;
  });

  it('initByLineConfig should start stack only once', function () {
    var lineCfg = {name: "lineName", password: "password", xivoIp: "xivoIP"};
    xc_webrtc.initByLineConfig(lineCfg, 'username', true, 8090);
    expect(sipml.init).toHaveBeenCalled();
    var backupFn = sipml.isReady;
    sipml.isReady = function() {return true;};
    readyCallback();
    expect(stack.start).toHaveBeenCalled();
    stack.start.calls.reset();
    xc_webrtc.initByLineConfig(lineCfg, 'username', true, 8090);
    expect(stack.start).not.toHaveBeenCalled();
    sipml.isReady = backupFn;
  });

    it('initByLineConfig throws exception if lineCfg.password is not set', function () {
        var lineCfg = {name: "lineName", xivoIp: "xivoIP"};
        expect( function() {
            xc_webrtc.initByLineConfig(lineCfg, 'username', true, 8090);
        } ).toThrow(new Error("Unable to configure WebRTC - LineConfig does not contains password"));
    });

    it('initByLineConfig should set correct wsserver if sipProxyName is not defined ', function () {
      var lineCfg = {name: "lineName", password: "password", xivoIp: "xivoIP"};
      xc_webrtc.initByLineConfig(lineCfg, 'username', true, 443, "audio_remote", "nginxsrv");
      readyCallback();
      expect(stackConfig.websocket_proxy_url).toBe("wss://nginxsrv:443/ws");
    });

    it('initByLineConfig should set correct wsserver when line is on main', function () {
      var lineCfg = {name: "lineName", password: "password", xivoIp: "xivoIP", sipProxyName: "default"};
      xc_webrtc.initByLineConfig(lineCfg, 'username', true, 443, "audio_remote", "nginxsrv");
      readyCallback();
      expect(stackConfig.websocket_proxy_url).toBe("wss://nginxsrv:443/ws");
    });

    it('initByLineConfig should set correct wsserver when line is on mds', function () {
      var lineCfg = {name: "lineName", password: "password", xivoIp: "xivoIP", sipProxyName: "mds1"};
      xc_webrtc.initByLineConfig(lineCfg, 'username', true, 443, "audio_remote", "nginxsrv");
      readyCallback();
      expect(stackConfig.websocket_proxy_url).toBe("wss://nginxsrv:443/ws-mds1");
    });

    it('stop should remove event callbacks and stop sip stack, session will be closed by framework', function () {
        // GIVEN:
        var lineCfg = {name: "lineName", password: "password", xivoIp: "xivoIP"};
        xc_webrtc.initByLineConfig(lineCfg, 'username', true, 8090);
        readyCallback();
        // WHEN:
        xc_webrtc.stop();
        // THEN:
        expect(session.removeEventListener).toHaveBeenCalledWith('*');
        expect(stack.removeEventListener).toHaveBeenCalledWith('*');
        expect(stack.stop).toHaveBeenCalledWith();
    });

    it('should not call hold/resume when the call is terminated', function() {
        var new_call_event = initIncomingSession([]);
        spyOn(new_call_event.newSession, 'accept').and.callThrough();
        generalEventListener(new_call_event );
        expect(xc_webrtc.answer()).toBe(true);
        expect(new_call_event.newSession.accept).toHaveBeenCalled();
        expect(xc_webrtc.hold()).toBe(true);
        expect(new_call_event.newSession.hold).toHaveBeenCalled();
        new_call_event.newSession.hold.calls.reset();
        var terminatedEvent = getTerminatedEvent(new_call_event.newSession.id);
        sessionEventListener(terminatedEvent);
        expect(xc_webrtc.hold()).toBe(false);
        expect(terminatedEvent.session.hold).not.toHaveBeenCalled();
        expect(terminatedEvent.session.resume).not.toHaveBeenCalled();

    });

    it('without initialized callSession, answer() returns false', function() {
        expect(xc_webrtc.answer()).toBe(false);
    });

    it('doesn\'t auto-answer the call if the autoAnswer header is NOT present', function() {
        var session = initIncomingSession([]);
        spyOn(session.newSession, 'accept').and.callThrough();
        generalEventListener(session);
        expect(session.newSession.accept).not.toHaveBeenCalled();
    });

    it('auto-answers the call if the autoAnswer header is present', function() {
        var newCallEvent = initIncomingSession(autoAnswerHeaders);
        spyOn(newCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(newCallEvent);
        expect(newCallEvent.newSession.accept).toHaveBeenCalled();
    });

    it('put second call on hold when auto answering a new call', function() {
        var firstCallSession = initFirstCall();
        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(firstCallSession.hold).toHaveBeenCalled();
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();
    });

    it('put second call on hold when answering without id', function() {
        var firstCallSession = initFirstCall();
        sendConnectedEvent(firstCallSession.id);

        var secondCallEvent = initNextIncomingSession([], 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(xc_webrtc.answer()).toBe(true);
        expect(firstCallSession.hold).toHaveBeenCalled();
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();
    });

    it('answer call by id', function() {
        initFirstCall();
        var secondCallEvent = initNextIncomingSession([], 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(xc_webrtc.answer(14)).toBe(true);
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();
    });

    it('answering an unexisting id returns false', function() {
        initFirstCall();
        var secondCallEvent = initNextIncomingSession([], 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(xc_webrtc.answer(15)).toBe(false);
        expect(xc_webrtc.answer(14)).toBe(true);
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();
    });

    it('hold call by id', function() {
        initFirstCall()
        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();

        expect(xc_webrtc.hold(14)).toBe(true);
        expect(secondCallEvent.newSession.hold).toHaveBeenCalled();
    });

    it('put current call on hold when initiating an attended transfer', function() {
        var firstCallSession = initFirstCall();
        sendConnectedEvent(firstCallSession.id);

        xc_webrtc.attendedTransfer('2002');
        expect(firstCallSession.hold).toHaveBeenCalled();
        expect(cti.attendedTransfer).toHaveBeenCalled();
    });

    it('do not put on hold if already have two callSession', function() {
        var firstCallSession = initFirstCall();
        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();

        sendConnectedEvent(firstCallSession.id);
        sendConnectedEvent(secondCallEvent.newSession.id);

        xc_webrtc.attendedTransfer('2002');
        expect(secondCallEvent.newSession.hold).not.toHaveBeenCalled();
    });

    it('proxy completeTransfer command to cti', function() {
        xc_webrtc.completeTransfer();
        expect(cti.completeTransfer).toHaveBeenCalled();
    });

    it('on hangup remove correct call from its internal state', function() {
        var firstCallEvent = initFirstCall();
        sendConnectedEvent(firstCallEvent.id);

        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();

        var terminatedEvent = getTerminatedEvent(14);
        sessionEventListener(terminatedEvent);

        expect(xc_webrtc.hold(14)).toBe(false);
        expect(xc_webrtc.hold()).toBe(true);
    });

    it('refuses to send dtmf when there\'s no established call', function() {
        var firstCallEvent = initFirstCall();

        expect(xc_webrtc.dtmf('1')).toBe(false);
    });

    it('refuses to send dtmf when more than one character is passed', function() {
        var firstCallSession = initFirstCall();
        sendConnectedEvent(firstCallSession.id);

        expect(xc_webrtc.dtmf('11')).toBe(false);
        expect(xc_webrtc.dtmf('1')).toBe(true);
        expect(firstCallSession.dtmf).toHaveBeenCalledWith('1')
    });

    it('sends dtmf on all active calls', function() {
        var firstCallSession = initFirstCall();
        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        expect(secondCallEvent.newSession.accept).toHaveBeenCalled();

        var thirdCallEvent = initNextIncomingSession([], 15);
        spyOn(thirdCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(thirdCallEvent);

        sendConnectedEvent(firstCallSession.id);
        sendConnectedEvent(secondCallEvent.newSession.id);

        expect(xc_webrtc.dtmf('5')).toBe(true);
        expect(firstCallSession.dtmf).toHaveBeenCalled();
        expect(secondCallEvent.newSession.dtmf).toHaveBeenCalledWith('5');
    });

    it('injects a new audio element on new incoming call', function() {
        var firstCallSession = initFirstCall();

        expect(document.getElementById('audio_remote' + firstCallSession.id) === null).toBe(false);
    });

    it('removes the audio element on hangup', function() {
        var firstCallSession = initFirstCall();

        expect(document.getElementById('audio_remote' + firstCallSession.id) === null).toBe(false);
        expect(xc_webrtc.internalAudioStream !== null).toBe(true);
        sessionEventListener(getTerminatedEvent(firstCallSession.id));
        expect(document.getElementById('audio_remote' + firstCallSession.id) === null).toBe(true);
    });

    it('injects a new audio element on new outgoing call', function() {
        initStack();
        var linecfg = {name: "lineName", password: "password", xivoIp: "xivoIP"};
        lineHandler(linecfg);
        expect(sipml.init).toHaveBeenCalled();
        readyCallback();
        expect(stack.start).toHaveBeenCalled();
        xc_webrtc.dial('1100');

        expect(document.getElementById('audio_remote' + session.getId()) === null).toBe(false);
    });

    it('injects a new audio element on second incoming call', function() {
        var firstCallSession = initFirstCall();
        expect(document.getElementById('audio_remote' + firstCallSession.id) === null).toBe(false);
        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14);
        generalEventListener(secondCallEvent);

        expect(document.getElementById('audio_remote' + secondCallEvent.newSession.id) === null).toBe(false);
    })

    it('removes the audio element on second incoming call hangup', function() {
        var firstCallSession = initFirstCall();
        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14);
        generalEventListener(secondCallEvent);

        sessionEventListener(getTerminatedEvent(secondCallEvent.newSession.id));
        expect(document.getElementById('audio_remote' + firstCallSession.id) === null).toBe(false);
        expect(document.getElementById('audio_remote' + secondCallEvent.newSession.id) === null).toBe(true);
    })

    it('registers session handler on new session notification', function () {
        var firstCallEvent = initIncomingSession([]);
        generalEventListener(firstCallEvent);

        expect(firstCallEvent.newSession.addEventListener).toHaveBeenCalledWith('*', sessionEventListener);
    })

    it('starts an audio call when the 2nd dial parameter is omitted', function() {
        initStack();
        xc_webrtc.dial('1100');
        expect(stack.newSession).toHaveBeenCalledWith('call-audio', {});
    })

    it('starts an audio call when the 2nd dial parameter is false', function() {
        initStack();
        xc_webrtc.dial('1100', false);
        expect(stack.newSession).toHaveBeenCalledWith('call-audio', {});
    })

    it('starts a video call if requested', function() {
        initStack();
        xc_webrtc.dial('1100', true);
        expect(stack.newSession).toHaveBeenCalledWith('call-audiovideo', {});
    })

    it('returns mediaType by sipCalId for incoming call', function() {
        initStack();
        var firstCallSession = initFirstCall('audio/video');
        expect(xc_webrtc.getMediaTypeBySipCallId(sipCallId)).toBe(xc_webrtc.mediaType.AUDIOVIDEO);
    })

    it('returns mediaType by sipCalId for outgoing call init by progress', function() {
        initStack();
        xc_webrtc.dial('1100', true);
        var progressEvent = getProgressEvent('outSessionId', 'audio/video');
        sessionEventListener(progressEvent);
        expect(xc_webrtc.getMediaTypeBySipCallId(sipCallId)).toBe(xc_webrtc.mediaType.AUDIOVIDEO);
    })

    it('updates elements on progress event', function() {
        initStack();
        xc_webrtc.dial('1100', true);
        var progressEvent = getProgressEvent('outSessionId', 'audio/video');
        sessionEventListener(progressEvent);
        expect(progressEvent.session.setConfiguration).toHaveBeenCalled();
    })

    it('updates elements on connected event', function() {
        initStack();
        xc_webrtc.dial('1100', true);
        var connectedEvent = getConnectedEvent('outSessionId', 'audio/video');
        sessionEventListener(connectedEvent);
        expect(connectedEvent.session.setConfiguration).toHaveBeenCalled();
    })

    it('provides a unique audio context',function() {
        var firstContext = xc_webrtc.getAudioContext();
        var secondContext = xc_webrtc.getAudioContext();
        expect(firstContext === secondContext).toBe(true)
    })

    it('returns the unregistered handler when the unregister method of localStreamHandlers is called', function() {
        var handler = jasmine.createSpy();
        var handlerBis = jasmine.createSpy();
        var unregisterMethod = xc_webrtc.setLocalStreamHandler(handler, "123-56");
        var unregisterMethodBis = xc_webrtc.setLocalStreamHandler(handlerBis, "123-56");
        var result = unregisterMethod();
        expect(result).toBe(handler);
        expect(unregisterMethodBis()).toBe(handlerBis);
    })

     it('returns the unregistered handler when the unregister method of remoteStreamHandlers is called', function() {
        var handler = jasmine.createSpy();
        var handlerBis = jasmine.createSpy();
        var unregisterMethod = xc_webrtc.setRemoteStreamHandler(handler, "123-56");
        var unregisterMethodBis = xc_webrtc.setRemoteStreamHandler(handlerBis, "123-56");
        var result = unregisterMethod();
        expect(result).toBe(handler);
        expect(unregisterMethodBis()).toBe(handlerBis);
    })

    it('propagates an event with the sipCallId of the session', function() {
        var handler = jasmine.createSpy();
        xc_webrtc.setHandler(xc_webrtc.MessageType.INCOMING, handler);
        xc_webrtc.setHandler(xc_webrtc.MessageType.OUTGOING, handler);
        xc_webrtc.dial();
        expect(handler).not.toHaveBeenCalledWith();

        sessionEventListener(getProgressEvent());
        sessionEventListener(getConnectedEvent('outSessionId'));
        sessionEventListener(getResumedEvent());
        sessionEventListener(getHoldedEvent());
        sessionEventListener(getTerminatedEvent('outSessionId'));

        expect(handler.calls.all()[0].args[0].type).toBe('Establishing');
        expect(handler.calls.all()[1].args[0].type).toBe('Ringing');
        expect(handler.calls.all()[2].args[0].type).toBe('Connected');
        expect(handler.calls.all()[3].args[0].type).toBe('Resume');
        expect(handler.calls.all()[4].args[0].type).toBe('Hold');
        expect(handler.calls.all()[5].args[0].type).toBe('Terminated');

        expect(handler.calls.all()[2].args[0].sipCallId).toBe(sipCallId);
        expect(handler.calls.all()[3].args[0].sipCallId).toBe(sipCallId);
        expect(handler.calls.all()[4].args[0].sipCallId).toBe(sipCallId);
        expect(handler.calls.all()[5].args[0].sipCallId).toBe(sipCallId);
    })

    it('answers on cti answer command', function() {
        initStack();
        var newCallEvent = initIncomingSession([]);
        generalEventListener(newCallEvent);
        spyOn(newCallEvent.newSession, 'accept').and.callThrough();
        ctiCmdHandler({'command':'Answer'});
        expect(newCallEvent.newSession.accept).toHaveBeenCalled();
    })

    it('put the call on hold on cti hold command', function() {
        var session = initFirstCall();
        sendConnectedEvent(session.id);
        ctiCmdHandler({'command':'Hold'});
        expect(session.hold).toHaveBeenCalled();
    })

    it('send dtmf on cti sendDtmf command', function() {
        var session = initFirstCall();
        sendConnectedEvent(session.id);
        ctiCmdHandler({'command':'SendDtmf', 'key': '7'});
        expect(session.dtmf).toHaveBeenCalledWith('7');
    })

    function startConference() {
        var firstCallSession = initFirstCall();
        sendConnectedEvent(firstCallSession.id);

        var secondCallEvent = initNextIncomingSession(autoAnswerHeaders, 14);
        spyOn(secondCallEvent.newSession, 'accept').and.callThrough();
        generalEventListener(secondCallEvent);
        sendConnectedEvent(secondCallEvent.newSession.id);

        expect(firstCallSession.hold).toHaveBeenCalled();
        sessionEventListener(getHoldedEvent(firstCallSession.id));

        xc_webrtc.conference();
        return {'firstCallSession': firstCallSession, 'secondCallSession': secondCallEvent.newSession};
    }

    it('deactivates hold on first session when a conference is started', function() {
        var confSessions = startConference();
        expect(confSessions.firstCallSession.resume).toHaveBeenCalled();
    })

    it('puts on hold both channels when a conference is put on hold', function() {
        var confSessions = startConference();
        sessionEventListener(getResumedEvent(confSessions.firstCallSession.id));
        xc_webrtc.hold();
        expect(confSessions.firstCallSession.hold.calls.count()).toBe(2);
        expect(confSessions.secondCallSession.hold).toHaveBeenCalled();
    })

    it('resumes both channels when a conference is resumed by calling conference once more', function() {
        var confSessions = startConference();
        sessionEventListener(getResumedEvent(confSessions.firstCallSession.id));
        xc_webrtc.hold();
        sessionEventListener(getHoldedEvent(confSessions.firstCallSession.id));
        sessionEventListener(getHoldedEvent(confSessions.secondCallSession.id));
        xc_webrtc.conference();
        expect(confSessions.firstCallSession.resume.calls.count()).toBe(2);
        expect(confSessions.secondCallSession.resume).toHaveBeenCalled();
    })

    it('resumes both channels when a conference is resumed by calling hold once more', function() {
        var confSessions = startConference();
        sessionEventListener(getResumedEvent(confSessions.firstCallSession.id));
        xc_webrtc.hold();
        sessionEventListener(getHoldedEvent(confSessions.firstCallSession.id));
        sessionEventListener(getHoldedEvent(confSessions.secondCallSession.id));
        xc_webrtc.hold();
        expect(confSessions.firstCallSession.resume.calls.count()).toBe(2);
        expect(confSessions.secondCallSession.resume).toHaveBeenCalled();
    })

});
